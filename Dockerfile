ARG ALPINE_BASE_IMAGE_NAME="docker.io/library/alpine"
ARG ALPINE_BASE_IMAGE_TAG="3.18"
FROM "${ALPINE_BASE_IMAGE_NAME}:${ALPINE_BASE_IMAGE_TAG}" AS final
RUN set -eux ; \
apk add --no-cache tar bind-tools xz gzip bzip2 curl rsync nano socat ;
