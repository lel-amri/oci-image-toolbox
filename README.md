An OCI image that I use as a general purpose toolbox for debugging containers or
pods. It can also be used as a base image for more specific toolboxes.
