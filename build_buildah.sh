#!/bin/sh
set -eu
print_usage() {
    printf "Usage: %s: [-b BASE_IMAGE_NAME] [-t BASE_IMAGE_TAG]\n" "$0"
}
alpine_base_image_name=
alpine_base_image_tag=
while getopts b:t: name
do
    case "${name}" in
    b)
        alpine_base_image_name="${OPTARG}"
        ;;
    t)
        alpine_base_image_tag="${OPTARG}"
        ;;
    ?)
        print_usage >&2
        exit 2
        ;;
    esac
done
set --
if [ -n "${alpine_base_image_name}" ] ; then
    set -- "$@" --build-arg "ALPINE_BASE_IMAGE_NAME=${alpine_base_image_name}"
fi
if [ -n "${alpine_base_image_tag}" ] ; then
    set -- "$@" --build-arg "ALPINE_BASE_IMAGE_TAG=${alpine_base_image_tag}"
fi
readonly iidfile="$(mktemp)"
trap "rm -f \"\${iidfile}\"" 0 2 3 15
buildah build --format oci --iidfile "${iidfile}" --file Dockerfile --ignorefile .dockerignore "$@" .
buildah push "$(cat "${iidfile}")" oci-archive:toolbox.tar
